<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::prefix('tandem')->group(function () {
    Route::get('dashboard', 'DashboardController@index')->name('tandem.dashboard');
    Route::get('products', 'ProductsController@index')->name('tandem.products');
    Route::get('resources', 'ResourcesController@index')->name('tandem.resources');
    Route::get('media', 'MediaController@index')->name('tandem.media');
    Route::get('mystuff', 'MyStuffController@index')->name('tandem.mystuff');
    Route::get('contacts', 'ContactsController@index')->name('tandem.contacts');
});

Auth::routes();

