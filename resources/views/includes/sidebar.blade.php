<div class="pt-4">
    <div class="sideMenuItem pl-4 py-2">
      <a href="{{ route('tandem.dashboard') }}">Dashboard</a>
    </div>  
    <div class="sideMenuItem pl-4 py-2">
      <a href="{{ route('tandem.products') }}">Products</a>
    </div> 
    <div class="sideMenuItem pl-4 py-2">
      <a href="{{ route('tandem.resources') }}">Resources</a>
    </div>     
    <div class="sideMenuItem pl-4 py-2">
      <a href="{{ route('tandem.media') }}">Media</a>
    </div> 
    <div class="sideMenuItem pl-4 py-2">
      <a href="{{ route('tandem.mystuff') }}">My Stuff</a>
    </div> 
    <div class="sideMenuItem pl-4 py-2">
      <a href="{{ route('tandem.contacts') }}">My Contacts</a>
    </div>
  </div>