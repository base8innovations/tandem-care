<div class="col-12 topnavbar">
    <div class="row">
        <div class="col-2">
            <img src="{{ asset('/images/sitelogo.png')}}" alt="" class="img-fluid sitelogo pt-3 pl-3">            
        </div>
        <div class="col-9 pt-3">
            <form action="#" class="searchForm">
                <input type="text" class="searchBox pl-4" placeholder="Search">
                <span class="searchIcon"><span class="mdi mdi-magnify"></span></span>
            </form>
        </div>
        <div class="col-1 pt-4">
            <h2><span class="initialsIcon p-2">KB</span></h2>
        </div>
    </div>
</div>