@extends('layouts.app')

@section('content')
<div>    
    <div class="row pb-3 pt-5">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage pt-2">
                <img src="{{ asset('/images/ppt.png') }}" alt="" style="max-height: 30px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Summer Time</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage pt-2">
                <img src="{{ asset('/images/note.png') }}" alt="" style="max-height: 30px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Singalong</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage pt-2">
                <img src="{{ asset('/images/interactive.png') }}" alt="" style="max-height: 40px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Picture game</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage pt-2">
                <img src="{{ asset('/images/pdf.png') }}" alt="" style="max-height: 30px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Holiday Alphabet Game</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage pt-2">
                <img src="{{ asset('/images/ppt.png') }}" alt="" style="max-height: 30px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Holiday Destination Quiz</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage pt-2">
                <img src="{{ asset('/images/pdf.png') }}" alt="" style="max-height: 30px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">The Seaside Holiday</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage pt-2">
                <img src="{{ asset('/images/pdf.png') }}" alt="" style="max-height: 30px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">The Summer Sun Round Me</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="resourceImage resourceImageSelected pt-2">
                <img src="{{ asset('/images/pdf.png') }}" alt="" style="max-height: 30px">
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Session Evaluation Sheet</h4>
        </div>
    </div>
    
</div>
@endsection
