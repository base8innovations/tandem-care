@extends('layouts.app')

@section('content')
<div>    
    <div class="row pb-3 pt-5">
        <div class="col-1 text-center pr-0">
            <div class="productIconOrange">
                <img src="{{ asset('/images/head.png') }}" alt="" class="pt-2" style="max-height: 75px">
            </div>
        </div>
        <div class="col-10">
            <div class="headerBackgroundOrange pl-2">
                <h4>Individualised Cognitive Stimulation Therapy</h4>
                <h5>Programme (14 sessions)</h5>
            </div>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="productIconGreen">
                <img src="{{ asset('/images/exit.png') }}" alt="" class="pt-2 pl-1" style="max-height: 75px">
            </div>
        </div>
        <div class="col-10">
            <div class="headerBackgroundGreen pl-2">
                <h4>Individualised Resilience Coaching Programme for Informal Carers</h4>
                <h5>Programme (12 sessions)</h5>
            </div>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="productIconPurple">
                <img src="{{ asset('/images/cog.png') }}" alt="" class="pt-2" style="max-height: 75px">
            </div>
        </div>
        <div class="col-10">
            <div class="headerBackgroundPurple pl-2">
                <h4>Ongoing Maintenance Therapy & Carer Coaching</h4>
                <h5>Coaching for informal carers</h5>
            </div>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="productIconBlue">
                <img src="{{ asset('/images/opendoor.png') }}" alt="" class="pt-2" style="max-height: 75px">
            </div>
        </div>
        <div class="col-10">
            <div class="headerBackgroundBlue pl-2">
                <h4>Membership portal</h4>
                <h5>Access to resources</h5>
            </div>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="productIconYellow">
                <img src="{{ asset('/images/gold.png') }}" alt="" class="pt-2" style="max-height: 75px">
            </div>
        </div>
        <div class="col-10">
            <div class="headerBackgroundYellow pl-2">
                <h4>Gold standard</h4>
                <h5>Healthy Habits</h5>
            </div>
        </div>
    </div>
</div>
@endsection
