@extends('layouts.app')

@section('content')
<div>    
    <div class="row pt-5 ml-0">
        <div class="col-3 tabActive contactTab">
            <h1 class="pl-2 pt-3">Team Tandem</h1>
        </div>
        <div class="col-3 contactTab mx-2">
            <h1 class="pl-2 pt-3">Informal Carers</h1>
        </div>
        <div class="col-3 contactTab">
            <h1 class="pl-2 pt-3">Patients</h1>
        </div>
    </div>
    <div class="contactsBackground mr-5">
        <div class="row">
            <div class="col-12 pt-5 pl-5">
                <form action="#">
                    <input type="text" class="searchBoxLarge pl-4" placeholder="Search">                   
                </form>
            </div>
        </div>
        <div class="row pt-5 pl-2">
            <div class="col-3 text-center">
                <img src="https://picsum.photos/200" class="largeContactCircle" alt="">
                <h4 class="pt-3" style="color: white">James Elwes</h4>
            </div>
            <div class="col-3 text-center">
                <img src="https://picsum.photos/200" class="largeContactCircle" alt="">
                <h4 class="pt-3" style="color: white">Kathrine Bailey</h4>
            </div>
            <div class="col-3 text-center">
                <img src="https://picsum.photos/200" class="largeContactCircle" alt="">
                <h4 class="pt-3" style="color: white">Jonathan Hanbury</h4>
            </div>
                        
        </div>
    </div>
</div>
@endsection
