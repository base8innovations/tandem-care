@extends('layouts.app')

@section('content')
<div>    
    <h4 class="pt-4">Wed 7 Oct</h4>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="discussion">19:00</div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Getting to know you 1:1</h4>
        </div>
    </div>
    <h4>Wed 14 Oct</h4>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="discussion">17:30</div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">Discussion to understand Joan</h4>
        </div>
    </div>
    <h4>Thu 15 Oct</h4>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="ICST">17:30</div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">ICST: Session 1: Individualized Resilience Coaching Programme</h4>
        </div>
    </div>
    <h4>Tues 20 Oct</h4>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="IRCP">
                due<br>
                17:30
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">IRCP: Session 1</h4>
        </div>
    </div>
    <h4>Tues 27 Oct</h4>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="ICST">17:30</div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">ICST: Session 1: Individualized Resilience Coaching Programme</h4>
        </div>
    </div>
    <h4>Wed 28 Oct</h4>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="IRCP">
                due<br>
                14:31
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">IRCP: Session 1</h4>
        </div>
    </div>
    <div class="row pb-3">
        <div class="col-1 text-center pr-0">
            <div class="IRCP">
                due<br>
                14:31
            </div>
        </div>
        <div class="col-10">
            <h4 class="dashboardHeaderBackground pl-2">IRCP: Session 2</h4>
        </div>
    </div>
</div>
@endsection
